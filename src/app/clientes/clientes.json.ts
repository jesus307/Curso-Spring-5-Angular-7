import { Cliente } from './cliente';

export const CLIENTES: Cliente[] = [
    {id: 1, nombre: 'Jesus', apellido: 'Linares', email: 'jesuslinares307@gmail.com', createAt: '2020-09-20'},
    {id: 2, nombre: 'Jesus2', apellido: 'Linares2', email: 'jesuslinares302@gmail.com', createAt: '2020-09-21'},
    {id: 3, nombre: 'Jesus3', apellido: 'Linares3', email: 'jesuslinares303@gmail.com', createAt: '2020-09-22'},
    {id: 4, nombre: 'Jesus4', apellido: 'Linares4', email: 'jesuslinares304@gmail.com', createAt: '2020-09-23'},
    {id: 5, nombre: 'Jesus5', apellido: 'Linares5', email: 'jesuslinares305@gmail.com', createAt: '2020-09-24'}
  ];